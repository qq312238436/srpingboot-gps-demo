package com.hgps.demo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hgps.demo.entity.BaseRespResult;
import com.hgps.demo.entity.GpsData;
import com.hgps.demo.util.ConfigUtil;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.util.DateUtils;

import java.awt.*;
import java.io.*;
import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试控制器
 *
 * @author: @我没有三颗心脏
 * @create: 2018-05-08-下午 16:46
 */
@Controller
public class saveRecordController {
    @RequestMapping(value = "/api/savegps.api",method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult savegps(@RequestBody GpsData data) {
       String path=  ConfigUtil.getKey("data.file.path");
       System.out.println(path+File.separatorChar+ ConfigUtil.DateFMT());
        File file=   new File(path);
        if(!file.exists()) {
            try {
                FileUtils.forceMkdir(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File dataFiles=  new File(file,  ConfigUtil.DateFMT()+".txt");
        System.out.println(dataFiles.getAbsoluteFile());
        try {
            org.apache.commons.io.IOUtils.write(JSON.toJSONString(data),new FileOutputStream(dataFiles));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  new BaseRespResult();
    }

    @RequestMapping(value = "/view/list",method = RequestMethod.GET)
    public String list(@RequestParam(value = "date") String date, Model model) throws IOException {
        String dir= "";
        String path=  ConfigUtil.getKey("data.file.path");
        System.out.println(path);
        File file=   new File(path);
        List<GpsData> list=new ArrayList<GpsData>();
        if(file.exists()) {

          File[] files=   file.listFiles();
            for (File f:files ) {
             String data= org.apache.commons.io.FileUtils.readFileToString(f,"UTF-8");
                GpsData gpsData=  JSON.parseObject(data,GpsData.class);
                gpsData.setFilename(f.getName());
                list.add(gpsData);
            }
        }
        model.addAttribute("list",list);
        return  "gpsList";
    }

    @RequestMapping(value = "/view/gps",method = RequestMethod.GET)
    public String gps( @RequestParam(value="filename") String filename,Model model) throws IOException {
        String dir= "";
        String path=  ConfigUtil.getKey("data.file.path");
       String jsonData= org.apache.commons.io.FileUtils.readFileToString(new File(path,filename));
       model.addAttribute( "data",   JSON.parseObject(jsonData,GpsData.class));
        return  "location";
    }
}
