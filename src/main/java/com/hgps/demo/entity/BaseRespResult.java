package com.hgps.demo.entity;

import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

/**
 * @author nelson
 */
public class BaseRespResult implements Serializable {

    public static final String FAIL_CODE = "110011";
    public static final String EXCEPTION_CODE = "服务器错误，请联系管理员！";
    private static final long serialVersionUID = 7713971056694686238L;
    protected String statuscode;
    protected String statusmsg;
    protected String openid;
    private Object data;

    public BaseRespResult() {
        this.statuscode = "0";
        this.statusmsg = "成功";
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatusmsg() {
        return statusmsg;
    }

    public void setStatusmsg(String statusmsg) {
        this.statusmsg = statusmsg;
    }

    public void setOkMsg(String statusmsg) {

        this.statusmsg = statusmsg;
    }

    public void setOkData(Object data) {
        this.data=data;
    }

    public void setErrMsg(String statusmsg) {
        this.statuscode = FAIL_CODE;
        this.statusmsg = statusmsg;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
