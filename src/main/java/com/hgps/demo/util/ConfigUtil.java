package com.hgps.demo.util;

import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * 读取配置文件
 */
public class ConfigUtil {

    private static Properties properties;
    static {
        try {
            properties=   PropertiesLoaderUtils.loadProperties(  new FileUrlResource( ResourceUtils.getURL("classpath:commom.properties")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getKey(String key){
        if(null!= properties){
             return   properties.getProperty(key);
        }
        return "";
    }

    public static String DateFMT(){
       DateFormat df=new SimpleDateFormat("yyyyMMdd-hhmmss");
      return   df.format(new Date());
    }
}
